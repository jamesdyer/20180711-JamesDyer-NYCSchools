//
//  School.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import Foundation

struct School: Codable {
    private var _id: String?
    private var _name: String?
    private var _totalStudents: String?
    private var _attendanceRate: String?
    private var _graduationRate: String?
    private var _grades: String?
    private var _phoneNumber: String?
    private var _website: String?
    private var _location: String?
    
    var satScores: SAT?
    
    private enum CodingKeys: String, CodingKey {
        case _id = "dbn"
        case _name = "school_name"
        case _totalStudents = "total_students"
        case _attendanceRate = "attendance_rate"
        case _graduationRate = "graduation_rate"
        case _grades = "grades2018"
        case _phoneNumber = "phone_number"
        case _website = "website"
        case _location = "location"
    }
    
    /**
     The id for the school and returns missing if no data was received.
     */
    var id: String {
        return _id ?? "Missing"
    }
    
    /**
     The of the school and returns unknown if no data was received.
     */
    var name: String {
        return _name ?? "Unknown"
    }
    
    /**
     The total amount of students at the school and returns unknown if no data was received.
     */
    var totalStudents: String {
        return _totalStudents ?? "Unknown"
    }
    
    /**
     The attendance rate at the school converted to a percentage and returns 0 if no data was received.
     */
    var attendanceRate: Double {
        if let attendRate = _attendanceRate, var adjustedRate = Double(attendRate) {
            adjustedRate *= 100
            return round(adjustedRate)
        } else {
            return 0
        }
    }
    
    /**
     The graduation rate at the school converted to a percentage and returns 0 if no data was received.
     */
    var graduationRate: Double {
        if let gradRate = _graduationRate, var adjustedRate = Double(gradRate) {
            adjustedRate *= 100
            return round(adjustedRate)
        } else {
            return 0
        }
    }
    
    /**
     The current grade levels that the school serves and returns unknown if no data was received or N/A if they don't have grade levels.
     */
    var grades: String {
        let currentGrades = _grades ?? "Unknown"
        return (currentGrades == "School is structured on credit needs, not grade level") ? "N/A" : currentGrades
    }
    
    /**
     The phone number to the school and returns unknown if no data was received.
     */
    var phoneNumber: String {
        return _phoneNumber ?? "Unknown"
    }
    
    /**
     The website to the school and returns unknown if no data was received.
     */
    var website: String {
        return _website ?? "Unknown"
    }
    
    /**
     The location of the school and returns unknown if no data was received.
     */
    var location: String {
        return _location ?? "Unknown"
    }
}
