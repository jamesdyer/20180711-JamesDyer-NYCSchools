//
//  SAT.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import Foundation

struct SAT: Codable {
    private let _id: String?
    private let _criticalReadingAverageScore: String?
    private let _mathAverageScore: String?
    private let _writingAverageScore: String?
    private let _testTakers: String?
    
    private enum CodingKeys: String, CodingKey {
        case _id = "dbn"
        case _criticalReadingAverageScore = "sat_critical_reading_avg_score"
        case _mathAverageScore = "sat_math_avg_score"
        case _writingAverageScore = "sat_writing_avg_score"
        case _testTakers = "num_of_sat_test_takers"
    }
    
    /**
     The id of the SAT that matches the school.
     */
    var id: String {
        return _id ?? "Missing"
    }
    
    /**
     The average score for the reading part of the SAT and returns missing if no data was received.
     */
    var criticalReadingAverageScore: String {
        return _criticalReadingAverageScore ?? "Missing"
    }
    
    /**
     The average score for the math part of the SAT and returns missing if no data was received.
     */
    var mathAverageScore: String {
        return _mathAverageScore ?? "Missing"
    }
    
    /**
     The average score for the writing part of the SAT and returns missing if no data was received.
     */
    var writingAverageScore: String {
        return _writingAverageScore ?? "Missing"
    }
    
    /**
     The amount of test takers for the SAT and returns missing if no data was received.
     */
    var testTakers: String {
        return _testTakers ?? "Missing"
    }
    
    /**
     The composite score for the SAT and returns 0 if no data was received or is not a number.
     
     - Returns: A double of the composite score.
     */
    func score() -> Double {
        guard let crAvgScore = Double(criticalReadingAverageScore), criticalReadingAverageScore != "Missing" else { return 0 }
        
        guard let mathAvgScore = Double(mathAverageScore), mathAverageScore != "Missing" else { return 0 }
        
        guard let writingAvgScore = Double(writingAverageScore), writingAverageScore != "Missing" else { return 0 }
        
        return  crAvgScore + mathAvgScore + writingAvgScore
    }
    
}
