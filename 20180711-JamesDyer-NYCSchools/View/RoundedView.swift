//
//  RoundedView.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/12/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    override func awakeFromNib() {
        layer.cornerRadius = 10
    }

}
