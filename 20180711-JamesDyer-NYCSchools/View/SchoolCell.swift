//
//  SchoolCell.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var gradesLabel: UILabel!
    @IBOutlet weak var totalStudentsLabel: UILabel!
    
    /**
     Configures the cell from the school.
     
     - Parameter school: The school to display for the cell.
     */
    func configureCell(for school: School) {
        schoolNameLabel.text = school.name
        gradesLabel.text = "Grades: \(school.grades)"
        totalStudentsLabel.text = "Students: \(school.totalStudents)"
    }

}
