//
//  DetailViewController.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var compSATScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!
    @IBOutlet weak var gradRateLabel: UILabel!
    @IBOutlet weak var attenRateLabel: UILabel!
    
    var school: School?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingIndicatorView.hidesWhenStopped = true
        loadingIndicatorView.startAnimating()

        if school != nil {
            loadSATData { success in
                if success {
                    self.updateView()
                }
            }
        } else {
            showError(with: "Couldn't find school!", dismiss: true)
        }
    }
    
    /**
     Shows an error to the user and dismiss the screen after.
     
     - Parameter message: The message to display to the user.
     - Parameter dismiss: Whether you want to pop the current view controller.
     */
    private func showError(with message: String, dismiss: Bool) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .default) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(dismiss)
        present(alert, animated: true, completion: nil)
    }
    
    /**
     Calls the CityOfNewYork API to load SAT data for the school.
     
     - Parameter completionHandler: The function to execute when the api call is done.
     - Parameter success: Whether the api call was successful
     */
    private func loadSATData(completionHandler: @escaping (_ success: Bool) -> ()) {
        CityOfNewYorkAPI.satScores { (data, response, error) in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    self.showError(with: "Unable to download data!", dismiss: false)
                    self.loadingIndicatorView.stopAnimating()
                    completionHandler(false)
                }
                return
            }
            
            do {
                var satScores: [SAT] = []
                satScores = try JSONDecoder().decode([SAT].self, from: data)
                
                for score in satScores {
                    if score.id == self.school!.id {
                        self.school!.satScores = score
                        break
                    }
                }
                
                DispatchQueue.main.async {
                    self.loadingIndicatorView.stopAnimating()
                    completionHandler(true)
                }
                
            } catch {
                DispatchQueue.main.async {
                    self.showError(with: "Failed to get accurate data!", dismiss: false)
                    self.loadingIndicatorView.stopAnimating()
                    completionHandler(false)
                }
            }
        }
    }
    
    /**
     Updates the view to show the details of the school.
     */
    private func updateView() {
        if let school = school {
            scrollView.isHidden = false
            nameLabel.text = school.name
            locationLabel.text = school.location
            phoneNumberLabel.text = school.phoneNumber
            websiteLabel.text = school.website
            gradRateLabel.text = String.init(format: "%g", school.graduationRate) + "%"
            attenRateLabel.text = String.init(format: "%g", school.attendanceRate) + "%"
            if let satScores = school.satScores {
                compSATScoreLabel.text = String.init(format: "%g", satScores.score())
                readingScoreLabel.text = satScores.criticalReadingAverageScore
                mathScoreLabel.text = satScores.mathAverageScore
                writingScoreLabel.text = satScores.writingAverageScore
                testTakersLabel.text = satScores.testTakers
            }
        }
    }

}
