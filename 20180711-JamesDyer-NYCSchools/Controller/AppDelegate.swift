//
//  AppDelegate.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }


}

