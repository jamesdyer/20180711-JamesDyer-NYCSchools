//
//  SchoolsViewController.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController {

    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var schoolTableView: UITableView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    var schools: [School] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        loadSchoolData {
            self.refreshButton.isEnabled = true
        }
    }
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        refreshButton.isEnabled = false
        schoolTableView.isHidden = true
        loadingIndicatorView.startAnimating()
        loadSchoolData {
            self.refreshButton.isEnabled = true
        }
    }
    
    /**
     Sets up the initial view.
     */
    private func setUpView() {
        refreshButton.isEnabled = false
        loadingIndicatorView.startAnimating()
        loadingIndicatorView.hidesWhenStopped = true
        schoolTableView.delegate = self
        schoolTableView.dataSource = self
    }
    
    /**
     Calls the CityOfNewYork API to load the schools data.
     
     - Parameter completionHandler: The function to execute when the api call is done.
     */
    private func loadSchoolData(completionHandler: @escaping () -> ()) {
        CityOfNewYorkAPI.schools { (data, response, error) in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    self.showError(with: "Unable to download data!")
                    self.loadingIndicatorView.stopAnimating()
                    completionHandler()
                }
                return
            }
            
            do {
                self.schools = try JSONDecoder().decode([School].self, from: data)
                
                DispatchQueue.main.async {
                    self.loadingIndicatorView.stopAnimating()
                    self.schoolTableView.isHidden = false
                    self.schoolTableView.reloadData()
                    completionHandler()
                }
                
            } catch {
                DispatchQueue.main.async {
                    self.showError(with: "Failed to get accurate data!")
                    self.loadingIndicatorView.stopAnimating()
                    completionHandler()
                }
            }
        }
    }
    
    /**
     Shows an error to the user.
     
     - Parameter message: The message to display to the user.
     */
    private func showError(with message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alert.addAction(dismiss)
        present(alert, animated: true, completion: nil)
    }

}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath) as? SchoolCell {
            cell.configureCell(for: schools[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "HomeToDetail", sender: schools[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToDetail" {
            if let detailViewController = segue.destination as? DetailViewController {
                if let school = sender as? School {
                    detailViewController.school = school
                }
            }
        }
    }
}

