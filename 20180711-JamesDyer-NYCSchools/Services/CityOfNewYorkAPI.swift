//
//  CityOfNewYorkAPI.swift
//  20180711-JD-NYCSchools
//
//  Created by James Dyer on 7/11/18.
//  Copyright © 2018 James Dyer. All rights reserved.
//

import Foundation

struct CityOfNewYorkAPI {
    
    /**
     Gets all the schools for the City of New York API.
     
     - Parameter completionHandler: The function to execute after the API call.
     - Parameter data: The data coming from the API call.
     - Parameter response: The url response of the API call.
     - Parameter error: The error from the API call.
     */
    static func schools(completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        let schoolsLocation = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
        
        guard let schoolsUrl = URL(string: schoolsLocation) else { return }
        
        URLSession.shared.dataTask(with: schoolsUrl) { (data, response, error) in
            completionHandler(data, response, error)
        }.resume()
    }
    
    /**
     Gets all the SAT scores for the City of New York API.
     
     - Parameter completionHandler: The function to execute after the API call.
     - Parameter data: The data coming from the API call.
     - Parameter response: The url response of the API call.
     - Parameter error: The error from the API call.
     */
    static func satScores(completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        let satLocation = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
        
        guard let schoolsUrl = URL(string: satLocation) else { return }
        
        URLSession.shared.dataTask(with: schoolsUrl) { (data, response, error) in
            completionHandler(data, response, error)
        }.resume()
    }
}
